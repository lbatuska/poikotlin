package com.example.poi.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.poi.data.database.entities.POIItem

@Database(
    entities = [POIItem::class],
    version = 1
)
abstract class POIDatabase : RoomDatabase() {

    abstract fun get_poi_data_access_object() : PoiDataAccessObject

    // basically static for creating a singleton
    companion object{

        @Volatile
        private  var instance : POIDatabase? = null;

        private val LOCK = Any();

        operator fun invoke(context: Context) = instance
            ?: synchronized(LOCK)
        {
            instance
                ?: createDatabase(
                    context
                )
                    .also { instance =  it }
        };

        private fun createDatabase(context : Context) =
                Room.databaseBuilder(context.applicationContext,
                    POIDatabase::class.java,
                    "database.db"
                    ).build();

    }

}