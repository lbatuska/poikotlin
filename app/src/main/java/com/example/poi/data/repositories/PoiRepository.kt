package com.example.poi.data.repositories

import com.example.poi.data.database.POIDatabase
import com.example.poi.data.database.entities.POIItem

class PoiRepository (
    private val db : POIDatabase
)
{

    suspend fun insert_or_update(item : POIItem) = db.get_poi_data_access_object().insert_or_update(item);

    suspend fun delete(item : POIItem) = db.get_poi_data_access_object().delete(item);

    fun get_all() = db.get_poi_data_access_object().get_all();

}